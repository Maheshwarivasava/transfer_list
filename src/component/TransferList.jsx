import React, { useState } from 'react';

const TransferList = () => {
    
    const [listItems, setListItems] = useState([
        { name: 'JS', list: 'left',checked:'false' },
        { name: 'HTML', list: 'left',checked:'false' },
        { name: 'CSS', list: 'left',checked:'false' },
        { name: 'TS', list: 'left' ,checked:'false'},
        { name: 'REACT', list: 'right' ,checked:'false'},
        { name: 'ANGULAR', list: 'right' ,checked:'false'},
        { name: 'VUE', list: 'right',checked:'false' },
        { name: 'SVELTE', list: 'right', checked:'false' }
    ]);
    

    const moveSelectedToLeft = () => {
        const updatedItems = listItems.map(item => {
            if (item.checked && item.list === 'right') {
                return { ...item, list: 'left' ,checked:false};
            }
            return item;
        });
        setListItems(updatedItems);
    };

    const moveSelectedToRight = () => {
        const updatedItems = listItems.map(item => {
            if (item.checked && item.list === 'left') {
                return { ...item, list: 'right' ,checked:false};
            }
            return item;
        });
        setListItems(updatedItems);
    };

    const moveAllToLeft = () => {
        const updatedItems = listItems.map(item => ({ ...item, list: 'left' }));
        setListItems(updatedItems);
    };

    const moveAllToRight = () => {
        const updatedItems = listItems.map(item => ({ ...item, list: 'right' }));
        setListItems(updatedItems);
    };

    const toggleItem = (name,checked) => {
       setListItems(prevItems =>
        prevItems.map(listItems=>
            listItems.name=== name ? { ...listItems, checked } : listItems
        ))
        
    };

    return (
        <div>
            <h2>Transfer List</h2>
            
            <div style={{ display: 'flex' }}>
                <div>
                    <h3>Left List</h3>
                    <ul>
                        {listItems.map((item, index) => (
                            item.list === 'left' && (
                                <li key={index}>
                                    <input
                                        type="checkbox"
                                        checked={checked}
                                        onChange={() => toggleItem(item.name,e.target.checked)}
                                    />
                                    {item.name}
                                </li>
                            )
                        ))}
                    </ul>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', height: '1rem', width: '10rem', margin: '3rem 2rem' }}>
                    <button onClick={moveAllToRight}>Move All to Right</button>
                    <button onClick={moveSelectedToLeft}>Move Selected to Left</button>
                    <button onClick={moveSelectedToRight}>Move Selected to Right</button>
                    <button onClick={moveAllToLeft}>Move All to Left</button>
                </div>
                <div>
                    <h3>Right List</h3>
                    <ul>
                        {listItems.map((item, index) => (
                            item.list === 'right' && (
                                <li key={index}>
                                    <input
                                        type="checkbox"
                                        checked={checked}
                                        onChange={() => toggleItem(item.name,e.target.checked)}
                                    />
                                    {item.name}
                                </li>
                            )
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default TransferList;
