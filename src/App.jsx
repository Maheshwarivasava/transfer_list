import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import TransferList from './component/TransferList'

function App() {

  return (
    <div>
      <TransferList/>
    </div>
  )
}

export default App
